
stages:
  - test
  - build
  - security-review
  - development
  - qa
  - prod

variables:
  IMAGE_DETAILS: image-details.txt

build:
  stage: test
  image: rustdocker/rust:nightly
  script:
    # - /root/.cargo/bin/cargo test
    - /root/.cargo/bin/cargo build --release
  artifacts:
    expire_in: 30 minutes
    paths:
      - target/release/hello_world
  cache:
    paths:
      - .cargo/
      - .cache/sccache

build-and-push-image:
  stage: build
  image: docker:19.03.8
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    FULL_IMAGE: gcr.io/${GOOGLE_PROJECT_ID}/hello-world
  services:
    - docker:19.03.8-dind
  script:
    - mkdir keys
    - echo $GOOGLE_BUILD_GSA | base64 -d > ${CI_PROJECT_DIR}/keys/key-file.json
    - cat ${CI_PROJECT_DIR}/keys/key-file.json | docker login -u _json_key --password-stdin https://gcr.io
    - docker build -t ${FULL_IMAGE}:${CI_COMMIT_SHORT_SHA} .
    - docker push ${FULL_IMAGE}:${CI_COMMIT_SHORT_SHA}
    - export IMAGE_ID=$(docker image inspect ${FULL_IMAGE}:${CI_COMMIT_SHORT_SHA} --format '{{ .ID }}')
    - |
        cat > ${IMAGE_DETAILS} <<EOL
        image: ${FULL_IMAGE} digest: ${IMAGE_ID}
        EOL
    - cat image-details.txt
  artifacts:
    expire_in: 24 hours
    paths:
      - image-details.txt

# docker image inspect gcr.io/$PROJECT_ID/binauthz-test:latest --format '{{ .ID }}' > image-digest.txt && cat image-digest.txt

security-audit:
  stage: security-review
  image: google/cloud-sdk:debian_component_based
  variables:
    ACTOR: "security"
  before_script:
    - mkdir keys
    - echo $GOOGLE_BUILD_GSA | base64 -d > ${CI_PROJECT_DIR}/keys/key-file.json
    - gcloud auth activate-service-account cicd-builds@${GOOGLE_PROJECT_ID}.iam.gserviceaccount.com --key-file=${CI_PROJECT_DIR}/keys/key-file.json
    - gcloud --quiet config set project ${GOOGLE_PROJECT_ID}
  script:
    - export KEYRING_NAME="$(gcloud kms keyrings list --location=us-central1 --format='value(NAME)')"
    # kms keyrings list produces a long string including the project and location, the below command only uses the keyring name
    - export KEYRING_NAME="${KEYRING_NAME##*/}"
    # Extract Image Path and Image Digest from generated file during image creation
    - export IMAGE_PATH="$(cat ${IMAGE_DETAILS} | awk '{print $2 }')"
    - export IMAGE_DIGEST="$(cat ${IMAGE_DETAILS} | awk '{print $4 }')"
    # - gcloud kms keys versions get-public-key key-version --location us-central1  --keyring ${KEYRING_NAME} --key ${ACTOR}-attestor-key --output-file ./output.pub
    - export PUBLIC_KEY_ID=$(gcloud container binauthz attestors describe ${ACTOR}-attestor --format='value(userOwnedGrafeasNote.publicKeys[0].id)')
    # Create the file to sign
    - gcloud container binauthz create-signature-payload --artifact-url=${IMAGE_PATH}@${IMAGE_DIGEST} > /tmp/generated_payload.json
    # Sign generated_payload.json with KMS
    - |
        gcloud kms asymmetric-sign \
        --location us-central1 \
        --keyring ${KEYRING_NAME} \
        --key ${ACTOR}-attestor-key \
        --version 1 \
        --digest-algorithm sha512 \
        --input-file /tmp/generated_payload.json \
        --signature-file /tmp/ec_signature

    # Create attestation
    - |
      gcloud container binauthz attestations create \
        --artifact-url="${IMAGE_PATH}@${IMAGE_DIGEST}" \
        --attestor="projects/${GOOGLE_PROJECT_ID}/attestors/${ACTOR}-attestor" \
        --signature-file=/tmp/ec_signature \
        --public-key-id="${PUBLIC_KEY_ID}"

  artifacts:
    when: always
    expire_in: 1 day
    paths:
    - /tmp/generated_payload.json
    - /tmp/ec_signature

deploy-development:
  stage: development
  environment:
    name: development
  script:
    - echo "Emulate deploy to development"

deploy-qa:
  stage: qa
  when: manual
  environment:
    name: qa
  script:
    - echo "Attest the image has been in Development and passed (Development Attestor)"
    - echo "Emulate deploy to qa"

deploy-production:
  stage: prod
  when: manual
  environment:
    name: prod
  script:
    - echo "Attest the image has been in QA and passed (QA Attestor)"
    - echo "Emulate deploy to prod"
